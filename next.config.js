/** @type {import('next').NextConfig} */
module.exports = {
  reactStrictMode: true,
  images: {
    domains: ['adventuresy.southeastasia.azurecontainer.io', 'cscentralindia100320012c.blob.core.windows.net']
  },
}
